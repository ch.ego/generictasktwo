package ru.codeinside.generics;

import java.util.ArrayList;
import java.util.List;

public class ValuesSeparator {
    List<Object> params;

    public ValuesSeparator(List<Object> params) {
        this.params = params;
    }

    public void addVariables(List<Object> inputValues) {
        params = inputValues;
    }

    public <T> List<Object> getSeparatedBy(Class<T> type) {
        List<Object> separated = new ArrayList<>();//инициализация для удаления предыдущих значений
        for (Object param: params){
            if (param.getClass() == type){
                separated.add(param);
            }
        }
        return separated;
    }
}
